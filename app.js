var express = require('express');
var bodyParser = require('body-parser');
var expressJWT = require('express-jwt');
var secrets = require('./secrets.json');
var cors = require('cors');

// database
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/trading_app');

// routes
var users = require('./routes/users');
var offers = require('./routes/offers');
var authentication = require('./routes/authentication');

var corsOptions = {
  origin: 'http://localhost:8080'
};

var app = express();

app.use(cors(corsOptions));

app.use(bodyParser.json({ limit: '2MB' }));
app.use(bodyParser.urlencoded({ extended: false }));

app.use(expressJWT({ secret: secrets.secret_key })
    .unless({ path: [
        '/api/login',
        { url: '/api/users', methods: ['POST'] },
        { url: /^\/api\/users\/.*/, methods: ['GET'] },
        { url: '/api/offers', methods: ['GET'] },
        { url: /^\/api\/offers\/.*/, methods: ['GET'] },
        { url: '/api/login', methods: ['POST'] }
    ] })
);

app.use('/api/users', users);
app.use('/api/offers', offers);
app.use('/api/login', authentication);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
        message: err.message,
        error: err
    }));
    res.end();    
});

app.listen(3000, function() {
    console.log('Listening on port 3000...')
});
