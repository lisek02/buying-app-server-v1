var mongoClient = require('mongodb').MongoClient;
var _mongodb;

var database  = {
    connect: function(callback) {
        mongoClient.connect('mongodb://localhost:27017/trading_app', function(err, db) {
            if (err) {
                throw err;
            } else {
                _mongodb = db;
                console.log('Connection estabilished');
                callback();
            }
        });  
    },  
    db: function() {
        return _mongodb;
    },
    close: function() {
        _mongodb.close();
    }
};

module.exports = database;