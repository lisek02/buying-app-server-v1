const express = require('express');
const mongoose = require('mongoose');
const User = require('./users');
const Schema = mongoose.Schema;

const offerSchema = new Schema({
    city: {
        type: String,
        trim: true,
        required: [true, 'City is required']
    },
    street: {
        type: String,
        trim: true,
        required: [true, 'Street name is required']
    },
    houseNumber: {
        type: Number,
        required: [true, 'House number is required']
    },
    aptNumber: {
        type: Number
    },
    picture: {
        type: String,
        trim: true,
        required: [true, 'Picture is required']  
    },
    rooms: {
        type: Number,
        required: [true, 'Number of rooms is required']
    },
    area: {
        type: Number,
        required: [true, 'Area is required']
    },
    floor: {
        type: Number,
        required: [true, 'Floor number is required']
    },
    balcony: {
        type: Boolean,
        default: false
    },
    description: {
        type: String,
        trim: true,
        required: [true, 'Description is required']
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = mongoose.model('Offer', offerSchema);