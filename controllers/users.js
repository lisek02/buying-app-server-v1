const User = require('../models/users');

const userController = {
    getAll: (request, response, next) => {
        User.find({}).exec((err, users) => {
            return response.json(users);
        });
    },
    
    getOne: (request, response, next) => {
        User.findOne({ _id: request.params.id }).exec((err, user) => {
            return response.json(user);
        });
    },
    
    save: (request, response, next) => {
        const user = new User({
            email: request.body.email,
            phoneNumber: request.body.phoneNumber,
            password: request.body.password,
            admin: request.body.admin ? request.body.admin : false
        });
        user.save((err) => {
            if (err) {
                switch (err.name) {
                    case 'ValidationError':
                    err.status = 400;
                    break;
                }
                return next(err);
            } else {
                return response.json({
                    _id: user._id,
                    email: user.email,
                    phoneNumber: user.phoneNumber,
                    admin: user.admin    
                }); 
            }
        });
    },
    
    update: (request, response, next) => {
        User.findOneAndUpdate({ _id: request.params.id }, request.body, { new: true }, (err, user) => {
            if (err) return next(err);
            if (!user) {
                err = new Error('Unprocessable Entity');
                err.status = 422;
                return next(err);
            } else {
                response.json(user);
            }
        });
    },
    
    delete: (request, response, next) => {
        User.findOneAndRemove({ _id: request.params.id }, (err, user) => {
            if (err) return next(err);
            if (!user) {
                err = new Error('Unprocessable Entity');
                err.status = 422;
                return next(err);
            } else {
                return response.json(user);
            }
        });
    }
}

module.exports = userController;