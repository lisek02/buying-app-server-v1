const Offer = require('../models/offers');
const User = require('../models/users');

const offerController = {
    getAll: (request, response, next) => {
        Offer.find({}).exec((err, offers) => {
            return response.json(offers);
        });
    },
    
    getOne: (request, response, next) => {
        Offer
        .findOne({ _id: request.params.id })
        .populate('owner')
        .exec((err, offer) => {
            return response.json(offer);
        });
    },
    
    save: (request, response, next) => {
        const user = User.findOne({ email: request.user.email }, (err, user) => {
            if (err) return next(err);
            if (!user) {
                err = new Error('Unprocessable Entity.');
                err.status = 422;
                return next(err);
            } else {
                const offer = new Offer(request.body);
                offer.owner = user._id;
                offer.save((err) => {
                    if (err) {
                        switch (err.name) {
                            case 'ValidationError':
                            err.status = 400;
                            break;
                        }
                        return next(err);
                    } else {
                        return response.json(offer); 
                    }
                });
            }
        });
    },
    
    update: (request, response, next) => {
        Offer.findOneAndUpdate({ _id: request.params.id }, request.body, { new: true }, (err, offer) => {
            if (err) return next(err);
            if (!offer) {
                err = new Error('Unprocessable Entity');
                err.status = 422;
                return next(err);
            } else {
                response.json(offer);
            }  
        });
    },
    
    delete: (request, response, next) => {
        Offer.findOneAndRemove({ _id: request.params.id }, (err, offer) => {
            if (err) return next(err);
            if (!offer) {
                err = new Error('Unprocessable Entity');
                err.status = 422;
                return next(err);
            } else {
                return response.json(offer);
            }
        });
    }
}

module.exports = offerController;