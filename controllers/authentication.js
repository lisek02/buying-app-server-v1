const jwt = require('jsonwebtoken');
const User = require('../models/users');
const secrets = require('../secrets.json');

const authenticationController = {
    authenticate: (request, response, next) => {
        User.findOne({ email: request.body.email }).exec((err, user) => {
            if (err) next(err);
            if (!user) {
                err = new Error('User not found');
                err.status = 404;
                return next(err);
            } else {
                if (user.password != request.body.password) {
                    err = new Error('Authentication failed');
                    err.status = 401;
                    return next(err);
                } else { 
                    const token = jwt.sign({ email: user.email }, secrets.secret_key, {
                        expiresIn: '24h'
                    });
                    return response.json({
                        user: user,
                        message: 'Success',
                        token: token
                    });
                }
            }
        });
    },
}

module.exports = authenticationController;