var express = require('express');
var router = express.Router();
var users = require('../controllers/users');

router.get('/', users.getAll);
router.get('/:id', users.getOne);
router.post('/', users.save);
router.put('/:id', users.update);
router.delete('/:id', users.delete);

module.exports = router;
