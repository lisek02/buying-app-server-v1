var express = require('express');
var router = express.Router();
var offers = require('../controllers/offers');

router.get('/', offers.getAll);
router.get('/:id', offers.getOne);
router.post('/', offers.save);
router.put('/:id', offers.update);
router.delete('/:id', offers.delete);

module.exports = router;
