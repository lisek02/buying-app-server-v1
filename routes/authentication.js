var express = require('express');
var router = express.Router();
var authentication = require('../controllers/authentication');

router.post('/', authentication.authenticate);

module.exports = router;
